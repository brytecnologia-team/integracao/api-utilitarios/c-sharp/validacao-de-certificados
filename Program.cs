using System;
using System.Security.Cryptography.X509Certificates;
using System.Net;
using Newtonsoft.Json;
using System.IO;

namespace Validação_de_certificados
{
    internal class Program
    {
        static string url = "https://fw2.bry.com.br/api/validador-service/v1/certificateReports";
        // https://URL_HUB_SIGNER/api/validador-service/v1/certificateReports

        static string caminhoCertificado = "./caminho/para/o/certificado/cert.cer";
        static string token = "Bearer insert-a-valid-token";

        static string mode = "CHAIN";
        static string extensionsReturn = "true";
        static string contentsReturn = "true";

        static Boolean token_verify()
        {
            if (token.Equals("Bearer insert-a-valid-token"))
            {
                Console.WriteLine("Configure a valid token");
                return false;
            }
            else
            {
                return true;
            }
        }

        static void cert_validate(string certificate)
        {
            Console.WriteLine("================ Initializing certificate verification ... ================");
            Console.WriteLine();

            string verification_form = "{   \"nonce\"                : \"" + 1 + "\" ," +
                                        "   \"mode\"                : \"" + mode + "\" ," +
                                        "   \"contentsReturn\"      : \"" + contentsReturn + "\" ," +
                                        "   \"extensionsReturn\"    : \"" + extensionsReturn + "\" ," +
                                        "   \"certificates\"        : [{" +
                                                                        "\"nonce\"          :   1," +
                                                                        "\"content\"    :   \"" + certificate + "\"" +
                                                                      "}]}";

            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
            webRequest.ContentType = "application/json";
            webRequest.Method = "POST";
            webRequest.Headers["Authorization"] = token;
            using (var streamWriter = new StreamWriter(webRequest.GetRequestStream()))
            {
                streamWriter.Write(verification_form);
            }
            try
            {
                var httpResponse = (HttpWebResponse)webRequest.GetResponse();
                // Se código é igual a 200
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    dynamic dynamicResult = JsonConvert.DeserializeObject(result);

                    dynamic chain_status = dynamicResult.reports[0].status;
                    int index = chain_status.certificateStatusList.Count - 1;
                    dynamic holder_certificate = chain_status.certificateStatusList[index];

                    Console.WriteLine("JSON response from the validator: " + dynamicResult);
                    Console.WriteLine();
                    Console.WriteLine("Holder informations: ");
                    if (chain_status != null && holder_certificate != null)
                    {
                        Console.WriteLine("Nome: " + holder_certificate.certificateInfo.subjectDN.cn);
                        Console.WriteLine("General status of the certificate chain: " + chain_status.status);
                        string holder_certificate_status = holder_certificate.status;
                        Console.WriteLine("Certificate status: " + holder_certificate_status);
                        if (!holder_certificate_status.Equals("VALID"))
                        {
                            Console.WriteLine(holder_certificate.errorStatus);
                            if (holder_certificate.revocationStatus != null)
                            {
                                Console.WriteLine(holder_certificate.revocationStatus);
                            }
                        }

                        Console.WriteLine("ICP-Brazil certificate: " + holder_certificate.pkiBrazil);
                        Console.WriteLine("Initial certificate expiration date: " + holder_certificate.certificateInfo.validity.notBefore);
                        Console.WriteLine("Final certificate expiration date: " + holder_certificate.certificateInfo.validity.notAfter);
                    } 
                    else
                    {
                        Console.WriteLine("Incomplete chain of the signatory: It was not possible to verify the certificate");
                        Console.WriteLine("Overall chain status " + chain_status.status);
                    }
                }
            }
            catch (WebException e)
            {
                using (var stream = e.Response.GetResponseStream())
                using (var reader = new StreamReader(stream))
                {
                    Console.WriteLine(reader.ReadToEnd());
                }
            }
        }

        static String getCertificado()
        {
            X509Certificate2 certificate = new X509Certificate2(caminhoCertificado);
            byte[] certBytes = certificate.RawData;

            return Convert.ToBase64String(certBytes);
        }

        static void Main(string[] args)
        {
            // Step 1 - Load the certificate to ve validated
            string certificate = getCertificado();

            if (token_verify())
            {
                // Step 2 - Send a request to the BRy service and print all the values ​​related to the certificate and its validity.
                cert_validate(certificate);
            }
        }
    }
}
